import java.util.Scanner;

/* This class used  calculate students gradings
 * @author Kobiga
 * @ version 7.0
 * @since 2020 June 16
 */
public class StudentsAutoGradingSystem {
	//helps to login teacher and student accounts
	public static boolean logIn(String user, String pwd, String[][] table, String identity) {

		boolean found = false;
		
		for (int i = 0; i < table.length; i++)
		{
			if (table[i][0].equals(user)) 
			{
				found = true;
				if (table[i][1].equals(pwd)) 
				{
					System.out.println(" You have successfully sign in : " + user + " as a " + identity);
					break;
				} else {
					System.out.println(" Incorrect Password, Please enter the correct password ");
					return false;
				}
			}
		}
		if (!found) {
			System.out.println("ERROR : " + identity + " does not exist in our system ");
			return false;

		} else {
			return true;
		}

	}
	//helps to print different subjecs based on id
	public static String getSubject(int sbj) {
		switch (sbj) {
		case 0:
			return "math";
		case 1:
			return "physics";
		case 2:
			return "chemistry";
		default:
			return "undefined";
		}
	}
    //helps to get the student ID
	public static int getStudent(String[][] students, String studentID) {
		for (int i = 0; i < students.length; i++) {
			if (students[i][0].equals(studentID)) {
				return i;
			}
		}
		return -1;
	}
	//helps to print a student report
	public static void printStudentReport(Double[][] table, String[][] students, int std) {
		double totalMarks = 0;
		System.out.print(students[std][0] + " ");
		for (int j = 0; j < table[std].length; j++) {
			Double value = table[std][j];
			totalMarks = totalMarks + value;
			System.out.print("| " + value + " |");
		}
		System.out.print(" " + getRank(totalMarks, 3)); // 3 stands for number of subjects
	}
	//helps to grade the students
	public static String getRank(Double mark, int n) {
		mark = mark / n;
		if (mark <= 100 && mark >= 75) {
			return "A";
		} else if (mark <= 74 && mark >= 65) {
			return "B";
		} else if (mark <= 64 && mark >= 55) {
			return "C";
		} else if (mark <= 54 && mark >= 35) {
			return "D";
		} else if (mark <= 34) {
			return "F";
		} else {
			return "N/A";
		}
	}
	//helps to print all the subjects
	public static void printSubject(Double[][] table, int subj) {
		System.out.println("Marks for " + getSubject(subj));
		System.out.println(" | Saranga | Mithra | Saraniya");
		for (int j = 0; j < table[subj].length; j++) {
			System.out.print("| " + table[j][subj] + " |");
		}
		System.out.println();
	}
    //helps to print the all students grades i
	public static void printMarkRecord(Double[][] markRecord, String[][] students) {
		System.out.println("userid | math | Physics | Chemistry | Grade");
		for (int i = 0; i < markRecord.length; i++) {
			printStudentReport(markRecord, students, i);
			System.out.println();
		}
	}
    //Set the initial value in the markRecord as 0
	public static Double[][] initalizeMarkRecord(Double[][] markRecord) {
		for (int i = 0; i < markRecord.length; i++) {
			for (int j = 0; j < markRecord[i].length; j++) {
				markRecord[i][j] = 0.0;
			}
		}
		return markRecord;
	}
	//helps to print student ids and name 
	public static String printStudents(String[][] std) {
		String str = "";
		for (int i = 0; i < std.length; i++) {
			str = str + (i + "-" + std[i][0] + ", ");
		}
		return str;
	}

	public static void main(String args[]) {

		String teachers[][] = { { "Kajooran", "K900" }, { "Velmurugan", "V8790" }, { "Raveendran", "VCM" } };
		String students[][] = { { "Saranga", "S07" }, { "Mithra", "M02" }, { "Saraniya", "S2611" } };
		Double markRecord[][] = new Double[3][3];
		boolean isTeacher = false;
		boolean isStudent = false;
		int studentID = -1;
		// intialize
		initalizeMarkRecord(markRecord);
		Scanner sc = new Scanner(System.in);

		do {
			// Teacher Login
			System.out.println("--------\" Welcome to Grading System \"-------------");
			System.out.println("Are you a teacher or a student?");
			String userInput = sc.next().trim();
			if (userInput.equalsIgnoreCase("teacher")) {
				// teacher login section
				isStudent = false;
				System.out.println("------// Teacher //-----");

				System.out.println("Teacher, Please Enter your username: ");
				String teacherName = sc.next().trim();

				System.out.println("Teacher, Please Enter your password:");
				String teacherPassword = sc.next().trim();

				isTeacher = logIn(teacherName, teacherPassword, teachers, "Teacher");
			} else if (userInput.equalsIgnoreCase("student")) {

				// Student login section
				isTeacher = false;
				System.out.println("------// Student //-----");

				System.out.println("Student, Please Enter your username: ");
				String studentName = sc.next().trim();

				System.out.println("Student, Please Enter your password:");
				String studentPassword = sc.next().trim();

				isStudent = logIn(studentName, studentPassword, students, "Student");
				studentID = getStudent(students, studentName);
			} else {
				System.out.println("Please type student or teacher. Your entry is invalid!");
				System.exit(0);
			}

			if (isTeacher) {
				do {
					System.out.println(
							"\n----------------------**************------------------**************--------------------------- \n"+
							 "Type\n 0 - add marks \n 1- find highesRankholder \n 2- find the marks for all students in specfic subject \n 3- print the all students record \n 4- remove a student \n "+
							"----------------------**************------------------**************---------------------------\n");
					int selectedMenu = sc.nextInt();

					switch (selectedMenu) {

					case 0:
						System.out.println("\" Add marks to student on each subject\"");

						do {

							System.out.println("Select the subject: 0-math, 1- physics, 2-chemistry ");
							int subjectID = sc.nextInt();

							System.out.println("Teacher, Please Enter studentId :  " + printStudents(students));
							int studentInputID = sc.nextInt();

							System.out.println("Teacher, Please Enter the mark: ");
							Double studentMark = sc.nextDouble();
							// add the mark to markRecord
							markRecord[studentInputID][subjectID] = studentMark;

							System.out.println("Teacher,Do you want to continue to enter mark: Type yes ");

						} while (sc.next().equalsIgnoreCase("yes"));
						break;
					case 1:
						System.out.println("--------------------------------------------------");

						System.out.println("\nFunction: see the hight mark holder on each subject \n");

						System.out.println("Teacher, Please Enter the subject: 0-math, 1- physics, 2-chemistry");
						int subjectID = Integer.parseInt(sc.next().trim());
						double max = -1;
						int highMarkHolder = -1;
						for (int i = 0; i < markRecord[subjectID].length; i++) {

							for (int j = 0; j < markRecord.length; j++) {
								double value = 0;
								if (markRecord[j][i] != null) {
									value = markRecord[j][i];
								}
								if (value > max) {
									max = value;
									highMarkHolder = j;
								}

							}

						}

						System.out.println("The highest mark for " + getSubject(subjectID) + " is : " + max
								+ " attained by " + students[highMarkHolder][0]);
						break;
					case 2:
						System.out.println("--------------------------------------------------");

						System.out.println("\nFunction: see the all students' marks on specific subject \n");
						System.out.println(
								"Teacher, Please Enter the subject: 0-math, 1- physics, 2-chemistry, 3-geography, 4-tamil ");
						subjectID = Integer.parseInt(sc.next().trim());
						printSubject(markRecord, subjectID);
						break;
					case 3:
						System.out.println("--------------------------------------------------");
						System.out.println("\nFunction: see the all students' marks on all subjects \n");
						printMarkRecord(markRecord, students);
						break;
					case 4:
						System.out.println("--------------------------------------------------");
						System.out.println("\nFunction: To remove a student from all subjects \n");
						System.out.println("Teacher, Please Enter studentId :" + printStudents(students));
						int studentToRemove = sc.nextInt();
						Double[][] newMarkRecord = new Double[markRecord.length - 1][3];
						for (int i = 0; i < newMarkRecord.length; i++) {
							for (int j = 0; j < newMarkRecord[i].length; j++) {
								if (i < studentToRemove) {
									newMarkRecord[i][j] = markRecord[i][j];
								} else {
									newMarkRecord[i][j] = markRecord[i + 1][j];
								}
							}

						}
						String[][] newStudents = new String[students.length - 1][2];
						for (int i = 0; i < newStudents.length; i++) {
							for (int j = 0; j < newStudents[i].length; j++) {
								if (i < studentToRemove) {
									newStudents[i][j] = students[i][j];
								} else {
									newStudents[i][j] = students[i + 1][j];
								}
							}

						}
						//record removed,so new record updated
						markRecord = newMarkRecord;
						students = newStudents;
						printMarkRecord(markRecord, students);

					}
					System.out.println("Do you want to try another function ?If so, Type yes");
				} while (sc.next().trim().equalsIgnoreCase("yes"));
			} else if (isStudent) {
				do {
					
					// student login
				System.out.println(
						"\n----------------------**************------------------**************--------------------------- \n"+
						 "Type\n 0 - see the report for all subjects \n 1- to see letter grade for selected subject \n" +
						"----------------------**************------------------**************---------------------------\n\n");
				int selectedMenu = sc.nextInt();

				switch (selectedMenu) {

				case 0:
				
					System.out.println("------REPORT CARD of " + students[studentID][0] +"---------");
					System.out.println("|Math|Physics|Chemistry|TotalGrade|"); 
					printStudentReport(markRecord, students, studentID);
					System.out.println();
					break;
					
				case 1:
					System.out.println("Student, Please enter the subject ID? 0-math");
					int selectedSubject = sc.nextInt();
					System.out.println ("The letter grade for subject is : "+ getRank(markRecord[studentID][selectedSubject],1) );
					break;
				default : System.out.println("invalid entry");
				}
				System.out.println("Do you want to try other funtions? Type yes");
				}while (sc.next().equalsIgnoreCase("yes"));
			}
			 else {
				System.out.println("Your login is not succesful. Please try again :)");
			}

			System.out.println("\n-------------Thank you for using the service-------------\n");

			System.out.println("Do you want to log out and try another user ?If so, Type yes");

		
		} while (sc.next().trim().equalsIgnoreCase("yes"));
	
		sc.close();
		System.exit(0);
	}

	

}
