


	  public class ParadigmsExample {
		    private int ID;
		    private String empName;
		    private int empAge;
		    

		    //Getter and Setter methods
		    public int getEmpID(){
		        return ID;
		    }

		    public String getEmpName(){
		        return empName;
		    }

		    public int getEmpAge(){
		        return empAge;
		    }

		    public void setEmpAge(int newValue){
		        empAge = newValue;
		    }

		    public void setEmpName(String newValue){
		        empName = newValue; 
		    }

		    public void setEmpID(int newValue){
		       ID = newValue;
		    }
		   
	    
		    public static void main(String args[]){
		    	ParadigmsExample obj = new ParadigmsExample();
		         obj.setEmpName("SushanSingh Rajput");
		         obj.setEmpAge(34);
		         obj.setEmpID(4848);
		         System.out.println("Employee Name: " + obj.getEmpName());
		         System.out.println("Employee ID: " + obj.getEmpID());
		         System.out.println("Employee Age: " + obj.getEmpAge());
		    } 
		


	
}
